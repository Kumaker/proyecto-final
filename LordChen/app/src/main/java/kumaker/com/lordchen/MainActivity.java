package kumaker.com.lordchen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    Button bSLog, bLog;
    EditText userLog, passLog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bLog=(Button)findViewById(R.id.Login_log);
        bSLog=(Button)findViewById(R.id.SignIn_Log);

        userLog=(EditText)findViewById(R.id.plain_user_log);
        passLog=(EditText)findViewById(R.id.plain_pass_log);

        bLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = userLog.getText().toString();
                String pass = passLog.getText().toString();
                if (!user.isEmpty() && !pass.isEmpty()) {
                    logIn("http://192.168.1.128:8080/lordchen/buscar_usuario.php", user, pass);
                } else {
                    Toast.makeText(MainActivity.this, "Porfavor, rellene los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void ejecutar_singin(View v){

        Intent i=new Intent(this, SingIn.class);

        startActivity(i);
    }

    public void logIn(String URL, final String User, final String Pass){

        StringRequest stringRequest=new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if(response.equals("120")){
                    Intent intent=new Intent(getBaseContext(), Principal.class);
                    startActivity(intent);
                } else {
                    if (response.equals("110")){
                        Toast.makeText(getBaseContext(), "Usuario no encontrado", Toast.LENGTH_SHORT).show();
                    } else if (response.equals("130")){
                        Toast.makeText(getBaseContext(), "Contraseña erronea", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getBaseContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parametros=new HashMap<String, String>();
                parametros.put("usuario",User);
                parametros.put("password",Pass);
                return parametros;
            }
        };

        RequestQueue requestQueue= Volley.newRequestQueue(getBaseContext());
        requestQueue.add(stringRequest);
    }
}
