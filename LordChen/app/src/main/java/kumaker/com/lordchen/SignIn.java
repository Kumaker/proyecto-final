package kumaker.com.lordchen;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

public class SingIn extends Activity {
    Button bSingIn;
    EditText nameSing, snameSing, userSing, passSing;
    String user, name, secondName, pass, url;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);

        bSingIn=(Button)findViewById(R.id.SingIn_sing);
        nameSing=(EditText)findViewById(R.id.plain_name_sing);
        snameSing=(EditText)findViewById(R.id.plain_sname_sing);
        userSing=(EditText)findViewById(R.id.plain_user_sing);
        passSing=(EditText)findViewById(R.id.plain_pass_sing);

        bSingIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                user = userSing.getText().toString();
                name = nameSing.getText().toString();
                secondName = snameSing.getText().toString();
                pass = passSing.getText().toString();
                url = "http://192.168.1.128:8080/lordchen/insertar_usuario.php";
                singIn(url, user, name, secondName,pass);
            }
        });
    }
    public void singIn(String URL, final String User, final String Name, final String SecondName, final String Pass){
        StringRequest stringRequest=new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(getApplicationContext(),"OPERACION EXITOSA", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parametros=new HashMap<String, String>();
                parametros.put("usuario",User);
                parametros.put("nombre",Name);
                parametros.put("apellido",SecondName);
                parametros.put("password",Pass);
                return parametros;
            }
        };
        RequestQueue requestQueue= Volley.newRequestQueue(getBaseContext());
        requestQueue.add(stringRequest);
    }
}
