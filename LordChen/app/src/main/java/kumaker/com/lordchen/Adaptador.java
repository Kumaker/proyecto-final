package kumaker.com.lordchen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Adaptador extends RecyclerView.Adapter<Adaptador.CartaHolder> {

    ArrayList<Carta> coleccion;

    public static class CartaHolder extends RecyclerView.ViewHolder {  // Cada dato es un String en este caso

        private TextView nombre;
        private TextView expansion;
        private TextView coste;
        private TextView cantidad;
        private ImageView foto;
        private View separador;
        private Context context;

        public CartaHolder(View itemView){
            super(itemView);
            nombre = itemView.findViewById(R.id.idNombre);
            expansion = itemView.findViewById(R.id.idExpansion);
            coste = itemView.findViewById(R.id.idMana);
            cantidad = itemView.findViewById(R.id.idCantidad);
            foto = itemView.findViewById(R.id.idImg);
            separador = itemView.findViewById(R.id.separador);
            this.context = context;
        }
        public void bindDatos(Carta carta) {
            nombre.setText(carta.getNombre());
            expansion.setText(carta.getExpansion());
            coste.setText(carta.getCoste());
            cantidad.setText("");
            /*if(carta.getURL().isEmpty()){
                foto.setVisibility((View.GONE));
            } else {
                foto.setVisibility(View.VISIBLE);
            }*/
            LinearLayout l=(LinearLayout) itemView.findViewById(R.id.layout_padre);
            LinearLayout ls=(LinearLayout) itemView.findViewById(R.id.layout_hijo);

            switch (carta.getColor()){
                case "W":
                    ls.setBackgroundColor(context.getResources().getColor(R.color.white,null));
                    break;
                case "B":
                    ls.setBackgroundColor(context.getResources().getColor(R.color.black,null));
                    break;
                case "U":
                    ls.setBackgroundColor(context.getResources().getColor(R.color.blue,null));
                    break;
                case "G":
                    ls.setBackgroundColor(context.getResources().getColor(R.color.green,null));
                    break;
                case "R":
                    ls.setBackgroundColor(context.getResources().getColor(R.color.red,null));
                    break;
                case "WG":
                case "GW":
                    ls.setBackground(context.getResources().getDrawable(R.drawable.selesnya_background,null));
                    break;
                case "WR":
                case "RW":
                    ls.setBackground(context.getResources().getDrawable(R.drawable.boros_background,null));
                    break;
                case "WU":
                case "UW":
                    ls.setBackground(context.getResources().getDrawable(R.drawable.arzorius_background,null));
                    break;
                case "WB":
                case "BW":
                    ls.setBackground(context.getResources().getDrawable(R.drawable.orzhov_background,null));
                    break;
                case "GR":
                case "RG":
                    ls.setBackground(context.getResources().getDrawable(R.drawable.gruul_background,null));
                    break;
                case "GU":
                case "UG":
                    ls.setBackground(context.getResources().getDrawable(R.drawable.simic_background,null));
                    break;
                case "GB":
                case "BG":
                    ls.setBackground(context.getResources().getDrawable(R.drawable.golgari_background,null));
                    break;
                case "RU":
                case "UR":
                    ls.setBackground(context.getResources().getDrawable(R.drawable.izzet_background,null));
                    break;
                case "BR":
                case "RB":
                    ls.setBackground(context.getResources().getDrawable(R.drawable.rakdos_background,null));
                    break;
                case "BU":
                case "UB":
                    ls.setBackground(context.getResources().getDrawable(R.drawable.dimir_background,null));
                    break;
                default:
                    if(carta.getColor().length()>2){
                        ls.setBackgroundColor(context.getResources().getColor(R.color.multi,null));
                    }else{
                        ls.setBackgroundColor(context.getResources().getColor(R.color.none,null));
                    }
                    break;
            }/*
            switch(carta.getRareza()){
                case "Common":
                    separador.setBackground(context.getResources().getDrawable(R.color.color_common,null));
                    l.setBackground(context.getResources().getDrawable(R.drawable.common_background,null));
                    break;
                case "Uncommon":
                    separador.setBackground(context.getResources().getDrawable(R.color.color_uncommon,null));
                    l.setBackground(context.getResources().getDrawable(R.drawable.uncommon_background,null));
                    break;
                case "Rare":
                    separador.setBackground(context.getResources().getDrawable(R.color.color_rare,null));
                    l.setBackground(context.getResources().getDrawable(R.drawable.rare_background,null));
                    break;
                case "Mythic":
                    separador.setBackground(context.getResources().getDrawable(R.color.color_mythic,null));
                    l.setBackground(context.getResources().getDrawable(R.drawable.mythic_background,null));
                    break;
            }*/
        }
    }

    public Adaptador(ArrayList<Carta> coleccion) {
        this.coleccion = coleccion;
    }

    @NonNull
    @Override
    public CartaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {  //Donde asignamos el layaut para la carta
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,null,false);
        CartaHolder cartas = new CartaHolder(view);
        return cartas;
    }

    @Override
    public void onBindViewHolder(@NonNull CartaHolder holder, int position) {  //Selecciona la carta donde la quieras tener
        holder.bindDatos(coleccion.get(position));
    }

    @Override
    public int getItemCount() {  //devuelve la cantidad de objetos que queremos meter en el recycle view
        return coleccion.size();
    }

    public void actualizar (ArrayList <Carta> cartas) {
      //  coleccion.clear();  //Metodo de los arrayList para vaciar el array
      //  coleccion = cartas;
        notifyDataSetChanged(); //Avisa al recycle view de que han cambiado los datos para que lo vuelva a ejecutar
    }
}
