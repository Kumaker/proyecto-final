package kumaker.com.lordchen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class Principal extends Activity {
    RecyclerView recycler;
    Button bColeccion, bSearch;
    EditText pSearch;
    ArrayList <Carta> coleccion = new ArrayList<Carta>();
    public static final int MY_DEFAULT_TIMEOUT = 15000; // Para que  a la hora de hacer la busqueda no de un TimeOut

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        bColeccion=(Button)findViewById(R.id.collection_prin);
        bSearch=(Button)findViewById(R.id.search_prin);
        pSearch=(EditText)findViewById(R.id.plain_search);
        recycler=(RecyclerView)findViewById(R.id.recicleID);

        recycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        bColeccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llamarColeccion();
            }
        });

        bSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = pSearch.getText().toString();
                buscar("https://api.magicthegathering.io/v1/cards?name="+name+"");
            }
        });

        Adaptador adapter = new Adaptador(coleccion);
        recycler.setAdapter(adapter);
    }

    public void llamarColeccion(){
        Intent intent = new Intent(this, Coleccion.class);
        startActivity(intent);
    }

    public void buscar (String URL){
    StringRequest stringRequest=new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);                   //Tratamos la respues de la api y la almacenamos en un objeto JSON
                JSONArray conjunto = jsonObject.getJSONArray("cards");                     //Transformamos el JSON en un array
                for (int i = 0; i<conjunto.length(); i++) {
                    Carta carta = new Carta();                                      //Creamos la carta
                    JSONObject jsCarta = conjunto.getJSONObject(i);                 //Generamos el objeto jsCarta segun la posicion de la array
                    carta.setNombre(jsCarta.getString("name"));              // Le pasamos a la clase carta el nombre del objeto jsCarta
                    if (jsCarta.has("multiverseid")) {
                        carta.setIdMultiverso(jsCarta.getInt("multiverseid"));
                    } else {
                        carta.setIdMultiverso(0);
                    }
                    if (jsCarta.has("number")) {
                        carta.setSetId(jsCarta.getString("number"));
                    } else {
                        carta.setSetId("0");
                    }

                    if (jsCarta.has("cmc")) {
                        carta.setCMC(jsCarta.getInt("cmc"));
                    } else {
                        carta.setCMC(0);
                    }
                    if (jsCarta.has("manaCost")) {
                        carta.setCoste(jsCarta.getString("manaCost"));
                    } else {
                        carta.setCoste("0");
                    }
                    if (jsCarta.has("text")){
                        carta.setDescripcion(jsCarta.getString("text"));
                    } else {
                        carta.setDescripcion("");
                    }
                    carta.setExpansion(jsCarta.getString("set"));
                    carta.setTipo(jsCarta.getString("type"));
                    if (jsCarta.has("power")) {
                        carta.setAtaque(jsCarta.getInt("power"));
                    } else {
                        carta.setAtaque(0);
                    }
                    if (jsCarta.has("toughness")) {
                        carta.setVida(jsCarta.getInt("toughness"));
                    } else {
                        carta.setVida(0);
                    }
                    if (jsCarta.has("imageUrl")) {
                        carta.setURL(jsCarta.getString("imageUrl"));
                    } else {
                        carta.setURL("");
                    }
                    if (jsCarta.has("rarity")) {
                        carta.setRareza(jsCarta.getString("rarity"));
                    } else {
                        carta.setRareza("");
                    }
                    if (jsCarta.has("colorIdentity")){
                        JSONArray colores = jsCarta.getJSONArray("colorIdentity");
                        if(colores.length()>0){
                            String color ="";
                            for (int a = 0; a < colores.length(); a++){
                                color = color.concat(colores.getString(a));
                            }
                            carta.setColor(color);
                        } else {
                            carta.setColor("");
                        }
                    } else {
                        carta.setColor("");
                    }
                    coleccion.add(carta);                                           //Añade la carta generada a la coleccion global.
                }
                Adaptador adaptador = (Adaptador) recycler.getAdapter();
                adaptador.actualizar(coleccion);
                recycler.setAdapter(adaptador);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getBaseContext(), error.toString(), Toast.LENGTH_SHORT).show();
        }
    }) {
        //protected Map<String, String> getParams() throws AuthFailureError {

       // }
    };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_DEFAULT_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        coleccion.clear();
        RequestQueue requestQueue= Volley.newRequestQueue(getBaseContext());
        requestQueue.add(stringRequest);
    }
}
