package kumaker.com.lordchen;

import androidx.recyclerview.widget.RecyclerView;

public class Carta  {

    int idMultiverso;
    int   CMC;
    int ataque;
    int vida;
    String nombre;
    String coste;
    String descripcion;
    String expansion;
    String tipo;
    String setId;
    String color;
    String URL;

    public String getRareza() {
        return Rareza;
    }

    public void setRareza(String rareza) {
        Rareza = rareza;
    }

    String Rareza;

    public int getIdMultiverso() {
        return idMultiverso;
    }

    public void setIdMultiverso(int idMultiverso) {
        this.idMultiverso = idMultiverso;
    }

    public String getSetId() {
        return setId;
    }

    public void setSetId(String setId) {
        this.setId = setId;
    }

    public int getCMC() {
        return CMC;
    }

    public void setCMC(int CMC) {
        this.CMC = CMC;
    }

    public int getAtaque() {
        return ataque;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCoste() {
        return coste;
    }

    public void setCoste(String coste) {
        this.coste = coste;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getExpansion() {
        return expansion;
    }

    public void setExpansion(String expansion) {
        this.expansion = expansion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

}
